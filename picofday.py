import os, requests, subprocess, urllib, urllib.request, os.path
from bs4 import BeautifulSoup
from urllib.parse import urlparse

url = requests.get("http://fuckinghomepage.com")
soup = BeautifulSoup(url.content)
for pic in soup.find_all("p"):
    if 'SWEET-ASS PICTURE' in ''.join(pic.findAll(text=True)):
        link = pic.find_next_sibling('p')
        if "http://" or "https://" in link.get('href', ''):
            link = link.find('small').find_next('a', href=True)['href']
            pic_of_day = urllib.parse.unquote(link.split('=')[1].split('&')[0])
            image = pic_of_day.split('/')[-1]
            if os.environ.get('DESKTOP_SESSION') == 'gnome':
                subprocess.call (["wget", "-P", "{}/Pictures/Wallpapers/images/".format(os.path.expanduser('~')), "-N", pic_of_day])
                subprocess.call (["gsettings", "set", "org.gnome.desktop.background", "picture-uri", "file:///{}/Pictures/Wallpapers/images/{}".format(os.path.expanduser('~'), image)])
            elif os.environ.get('DESKTOP_SESSION') == 'plasma':
                subprocess.call (["wget", "-P", "{}/Pictures/Wallpapers/images/".format(os.path.expanduser('~')), "-N", pic_of_day])

                script ="""dbus-send --session --dest=org.kde.plasmashell --type=method_call /PlasmaShell org.kde.PlasmaShell.evaluateScript 'string:
                var Desktops = desktops();
                for (i=0;i<Desktops.length;i++) {
                        d = Desktops[i];
                        d.wallpaperPlugin = "org.kde.image";
                        d.currentConfigGroup = Array("Wallpaper",
                                                    "org.kde.image",
                                                    "General");"""
                string1="""\n\t\t\td.writeConfig("Image", "file:///{}/Pictures/Wallpapers/images/{}");"""
                string2="""\n\t\t}'"""
                kde = open('/tmp/script.sh', 'w')
                kde.write(script)
                kde.write(string1.format(os.path.expanduser('~'), image))
                kde.write(string2)
                kde.close()
                os.chmod('/tmp/script.sh', 455)
                subprocess.Popen (['bash', '/tmp/script.sh'])
            else:
                print(os.environ.get('DESKTOP_SESSION'))
